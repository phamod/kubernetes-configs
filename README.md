# Kubernetes configs
a group of congiguration files to help deploy an application cluster and a pipeline cluster.

## kubernetes information

### copying files to pod
kubectl cp ./post-receive \<pod name>:/tmp/ -c \<container name>

### applying configs
the expectation is that the repo is cloned down but configs can be applied directly from the repo using the url from gitlab.
> kubectl apply -f \<file name>

###  starting up jenkins
> kubectl apply -f windows/jenkins-volume.yaml  -f shared/jenkins-claim.yaml -f shared/jenkins.yaml 

### starting gitlab
> kubectl apply -f windows/gitlab-volumes.yaml  -f shared/gitlab-claims.yaml -f shared/gitlab.yaml

### starting sonarqube
> kubectl apply -f windows/sonarqube-volume.yaml  -f shared/sonarqube-claim.yaml -f shared/sonarqube-postgresql.yaml  -f shared/sonarqube.yaml